﻿using Microsoft.VisualBasic;
using P3.APP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P3.UI
{
    public partial class Form_MainScreen : Form
    {
        GameSettings gameSettings;
        Form_Game form_game;

        public Form_MainScreen()
        {
            InitializeComponent();
            gameSettings = new GameSettings();
        }

        private void Form_MainScreen_Load(object sender, EventArgs e)
        {
            LoadSettings(LoadGame());
        }
        private Form_Game LoadGame()
        {
            form_game = new Form_Game();
            form_game.MdiParent = this;
            form_game.WindowState = FormWindowState.Maximized;
            form_game.Show();
            toolStripButton_Settings.Enabled = true;
            return form_game;
        }
        private void LoadSettings(Form_Game fg)
        {
            gameSettings = fg.GM.Settings;
        }
        public void CloseSettings(Form_Settings f)
        {
            EnableSettings();
            f.Close();
        }
        public void SaveSettings(Form_Settings f)
        {
            foreach (Control c in f.Controls)
            {
                if (c is TextBox)
                {
                    switch (c.Name)
                    {
                        case "textBox_QuestionsPerGame":
                            form_game.GM.Settings.UpdateQuestionsPerGame(Int32.Parse(c.Text));
                            break;
                        case "textBox_LivesPerGame":
                            form_game.GM.Settings.UpdateLivesPerGame(Int32.Parse(c.Text));
                            break;
                        case "textBox_TimePerRound":
                            form_game.GM.Settings.UpdateTimePerRound(Int32.Parse(c.Text));
                            break;
                    }
                }
            }
            CloseSettings(f);
        }
        private void toolStripButton_Settings_Click(object sender, EventArgs e)
        {
            Form_Settings form_Settings = new Form_Settings(gameSettings);
            form_Settings.MdiParent = this;
            form_Settings.WindowState = FormWindowState.Maximized;
            form_Settings.Show();
            DisableSettings();
        }
        public void EnableSettings()
        {
            toolStripButton_Settings.Enabled = true;
        }
        public void DisableSettings()
        {
            toolStripButton_Settings.Enabled = false;
        }
    }
}