﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using P3.APP;
using P3.BLL;

namespace P3.UI
{
    public partial class Form_Settings : Form
    {
        GameSettings Settings;

        public Form_Settings(GameSettings settings)
        {
            InitializeComponent();
            Settings = settings;
        }

        private void Form_Settings_Load(object sender, EventArgs e)
        {
            textBox_QuestionsPerGame.Text = Settings.QuestionsPerGame.ToString();
            textBox_LivesPerGame.Text = Settings.LivesPerGame.ToString();
            textBox_TimePerRound.Text = Settings.TimePerRound.ToString();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            Form_MainScreen temp = (Form_MainScreen)ParentForm;
            temp.CloseSettings(this);
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            try
            {
                int i;
                if (!int.TryParse(textBox_QuestionsPerGame.Text, out i) || (i < 1) || (i > Settings.QuestionsAmount))
                {
                    textBox_QuestionsPerGame.BackColor = Color.IndianRed;
                    throw new Exception($"Ingrese un numero valido de Preguntas por Partida!\n(Numero Entero Positivo, entre 1 y {Settings.QuestionsAmount}).");
                }
                if (!int.TryParse(textBox_LivesPerGame.Text, out i) || (i < 0))
                {
                    textBox_LivesPerGame.BackColor = Color.IndianRed;
                    throw new Exception("Ingrese un numero valido de Vidas por Partida\n(Numero Entero Positivo).");
                }
                if (!int.TryParse(textBox_TimePerRound.Text, out i) || (i < 0))
                {
                    textBox_TimePerRound.BackColor = Color.IndianRed;
                    throw new Exception("Ingrese un numbero valido de Tiempo por Ronda!\n(Numero Entero Positivo)");
                }
                Form_MainScreen temp = (Form_MainScreen)ParentForm;
                temp.SaveSettings(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
