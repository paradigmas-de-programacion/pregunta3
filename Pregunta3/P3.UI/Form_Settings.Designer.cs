﻿namespace P3.UI
{
    partial class Form_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_QuestionsPerGame = new System.Windows.Forms.Label();
            this.textBox_QuestionsPerGame = new System.Windows.Forms.TextBox();
            this.label_TimePerRound = new System.Windows.Forms.Label();
            this.textBox_TimePerRound = new System.Windows.Forms.TextBox();
            this.label_LivesPerGame = new System.Windows.Forms.Label();
            this.textBox_LivesPerGame = new System.Windows.Forms.TextBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_QuestionsPerGame
            // 
            this.label_QuestionsPerGame.AutoSize = true;
            this.label_QuestionsPerGame.Location = new System.Drawing.Point(13, 13);
            this.label_QuestionsPerGame.Name = "label_QuestionsPerGame";
            this.label_QuestionsPerGame.Size = new System.Drawing.Size(112, 13);
            this.label_QuestionsPerGame.TabIndex = 0;
            this.label_QuestionsPerGame.Text = "Preguntas por Partida:";
            // 
            // textBox_QuestionsPerGame
            // 
            this.textBox_QuestionsPerGame.Location = new System.Drawing.Point(132, 9);
            this.textBox_QuestionsPerGame.Name = "textBox_QuestionsPerGame";
            this.textBox_QuestionsPerGame.Size = new System.Drawing.Size(106, 20);
            this.textBox_QuestionsPerGame.TabIndex = 0;
            // 
            // label_TimePerRound
            // 
            this.label_TimePerRound.AutoSize = true;
            this.label_TimePerRound.Location = new System.Drawing.Point(13, 39);
            this.label_TimePerRound.Name = "label_TimePerRound";
            this.label_TimePerRound.Size = new System.Drawing.Size(98, 13);
            this.label_TimePerRound.TabIndex = 0;
            this.label_TimePerRound.Text = "Tiempo por Ronda:";
            // 
            // textBox_TimePerRound
            // 
            this.textBox_TimePerRound.Location = new System.Drawing.Point(132, 35);
            this.textBox_TimePerRound.Name = "textBox_TimePerRound";
            this.textBox_TimePerRound.Size = new System.Drawing.Size(106, 20);
            this.textBox_TimePerRound.TabIndex = 1;
            // 
            // label_LivesPerGame
            // 
            this.label_LivesPerGame.AutoSize = true;
            this.label_LivesPerGame.Location = new System.Drawing.Point(13, 65);
            this.label_LivesPerGame.Name = "label_LivesPerGame";
            this.label_LivesPerGame.Size = new System.Drawing.Size(90, 13);
            this.label_LivesPerGame.TabIndex = 0;
            this.label_LivesPerGame.Text = "Vidas por Partida:";
            // 
            // textBox_LivesPerGame
            // 
            this.textBox_LivesPerGame.Location = new System.Drawing.Point(132, 61);
            this.textBox_LivesPerGame.Name = "textBox_LivesPerGame";
            this.textBox_LivesPerGame.Size = new System.Drawing.Size(106, 20);
            this.textBox_LivesPerGame.TabIndex = 2;
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(163, 87);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(75, 23);
            this.button_Save.TabIndex = 4;
            this.button_Save.Text = "Guardar";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(82, 87);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "Cancelar";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // Form_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 117);
            this.ControlBox = false;
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.textBox_LivesPerGame);
            this.Controls.Add(this.label_LivesPerGame);
            this.Controls.Add(this.textBox_TimePerRound);
            this.Controls.Add(this.label_TimePerRound);
            this.Controls.Add(this.textBox_QuestionsPerGame);
            this.Controls.Add(this.label_QuestionsPerGame);
            this.MinimizeBox = false;
            this.Name = "Form_Settings";
            this.Text = "Settings";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form_Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_QuestionsPerGame;
        private System.Windows.Forms.TextBox textBox_QuestionsPerGame;
        private System.Windows.Forms.Label label_TimePerRound;
        private System.Windows.Forms.TextBox textBox_TimePerRound;
        private System.Windows.Forms.Label label_LivesPerGame;
        private System.Windows.Forms.TextBox textBox_LivesPerGame;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Button button_Cancel;
    }
}