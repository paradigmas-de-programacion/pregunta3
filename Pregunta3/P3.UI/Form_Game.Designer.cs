﻿namespace P3.UI
{
    partial class Form_Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel_GameView = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button_Play = new System.Windows.Forms.Button();
            this.tableLayoutPanel_PlayersView = new System.Windows.Forms.TableLayoutPanel();
            this.panel_Player2 = new System.Windows.Forms.Panel();
            this.label_ScorePlayer2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel_AnswersPlayer2 = new System.Windows.Forms.TableLayoutPanel();
            this.button_1Player2 = new System.Windows.Forms.Button();
            this.button_2Player2 = new System.Windows.Forms.Button();
            this.button_3Player2 = new System.Windows.Forms.Button();
            this.button_4Player2 = new System.Windows.Forms.Button();
            this.label_LivesPlayer2 = new System.Windows.Forms.Label();
            this.label_NamePlayer2 = new System.Windows.Forms.Label();
            this.panel_Player1 = new System.Windows.Forms.Panel();
            this.label_ScorePlayer1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel_AnswersPlayer1 = new System.Windows.Forms.TableLayoutPanel();
            this.button_1Player1 = new System.Windows.Forms.Button();
            this.button_2Player1 = new System.Windows.Forms.Button();
            this.button_3Player1 = new System.Windows.Forms.Button();
            this.button_4Player1 = new System.Windows.Forms.Button();
            this.label_LivesPlayer1 = new System.Windows.Forms.Label();
            this.label_NamePlayer1 = new System.Windows.Forms.Label();
            this.panel_Questions = new System.Windows.Forms.Panel();
            this.tableLayoutPanel_Answers = new System.Windows.Forms.TableLayoutPanel();
            this.label_Answer1 = new System.Windows.Forms.Label();
            this.label_Answer2 = new System.Windows.Forms.Label();
            this.label_Answer4 = new System.Windows.Forms.Label();
            this.label_Answer3 = new System.Windows.Forms.Label();
            this.label_Question = new System.Windows.Forms.Label();
            this.tableLayoutPanel_RoundInfo = new System.Windows.Forms.TableLayoutPanel();
            this.label_Category = new System.Windows.Forms.Label();
            this.label_TimeLeft = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel_GameView.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel_PlayersView.SuspendLayout();
            this.panel_Player2.SuspendLayout();
            this.tableLayoutPanel_AnswersPlayer2.SuspendLayout();
            this.panel_Player1.SuspendLayout();
            this.tableLayoutPanel_AnswersPlayer1.SuspendLayout();
            this.panel_Questions.SuspendLayout();
            this.tableLayoutPanel_Answers.SuspendLayout();
            this.tableLayoutPanel_RoundInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_GameView
            // 
            this.panel_GameView.AutoSize = true;
            this.panel_GameView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panel_GameView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_GameView.Controls.Add(this.tableLayoutPanel1);
            this.panel_GameView.Controls.Add(this.tableLayoutPanel_PlayersView);
            this.panel_GameView.Controls.Add(this.panel_Questions);
            this.panel_GameView.Controls.Add(this.label_Question);
            this.panel_GameView.Controls.Add(this.tableLayoutPanel_RoundInfo);
            this.panel_GameView.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_GameView.Location = new System.Drawing.Point(0, 0);
            this.panel_GameView.Name = "panel_GameView";
            this.panel_GameView.Padding = new System.Windows.Forms.Padding(25);
            this.panel_GameView.Size = new System.Drawing.Size(514, 603);
            this.panel_GameView.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.button_Play, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(25, 524);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(460, 50);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // button_Play
            // 
            this.button_Play.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Play.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Play.Location = new System.Drawing.Point(118, 13);
            this.button_Play.Name = "button_Play";
            this.button_Play.Size = new System.Drawing.Size(224, 34);
            this.button_Play.TabIndex = 3;
            this.button_Play.Text = "Ingresar Jugadores";
            this.button_Play.UseVisualStyleBackColor = true;
            this.button_Play.Click += new System.EventHandler(this.button_PlayPause_Click);
            this.button_Play.KeyDown += new System.Windows.Forms.KeyEventHandler(this.button_Play_KeyDown);
            // 
            // tableLayoutPanel_PlayersView
            // 
            this.tableLayoutPanel_PlayersView.AutoSize = true;
            this.tableLayoutPanel_PlayersView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel_PlayersView.ColumnCount = 2;
            this.tableLayoutPanel_PlayersView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_PlayersView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_PlayersView.Controls.Add(this.panel_Player2, 1, 0);
            this.tableLayoutPanel_PlayersView.Controls.Add(this.panel_Player1, 0, 0);
            this.tableLayoutPanel_PlayersView.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel_PlayersView.Location = new System.Drawing.Point(25, 295);
            this.tableLayoutPanel_PlayersView.Name = "tableLayoutPanel_PlayersView";
            this.tableLayoutPanel_PlayersView.RowCount = 1;
            this.tableLayoutPanel_PlayersView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel_PlayersView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 183F));
            this.tableLayoutPanel_PlayersView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 183F));
            this.tableLayoutPanel_PlayersView.Size = new System.Drawing.Size(460, 229);
            this.tableLayoutPanel_PlayersView.TabIndex = 0;
            // 
            // panel_Player2
            // 
            this.panel_Player2.AutoSize = true;
            this.panel_Player2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Player2.Controls.Add(this.label_ScorePlayer2);
            this.panel_Player2.Controls.Add(this.tableLayoutPanel_AnswersPlayer2);
            this.panel_Player2.Controls.Add(this.label_LivesPlayer2);
            this.panel_Player2.Controls.Add(this.label_NamePlayer2);
            this.panel_Player2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Player2.Location = new System.Drawing.Point(230, 0);
            this.panel_Player2.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Player2.Name = "panel_Player2";
            this.panel_Player2.Size = new System.Drawing.Size(230, 229);
            this.panel_Player2.TabIndex = 1;
            // 
            // label_ScorePlayer2
            // 
            this.label_ScorePlayer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_ScorePlayer2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_ScorePlayer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ScorePlayer2.Location = new System.Drawing.Point(0, 179);
            this.label_ScorePlayer2.Name = "label_ScorePlayer2";
            this.label_ScorePlayer2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.label_ScorePlayer2.Size = new System.Drawing.Size(226, 46);
            this.label_ScorePlayer2.TabIndex = 3;
            this.label_ScorePlayer2.Text = "Puntaje: 0";
            this.label_ScorePlayer2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel_AnswersPlayer2
            // 
            this.tableLayoutPanel_AnswersPlayer2.AutoSize = true;
            this.tableLayoutPanel_AnswersPlayer2.ColumnCount = 4;
            this.tableLayoutPanel_AnswersPlayer2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer2.Controls.Add(this.button_1Player2, 1, 0);
            this.tableLayoutPanel_AnswersPlayer2.Controls.Add(this.button_2Player2, 0, 1);
            this.tableLayoutPanel_AnswersPlayer2.Controls.Add(this.button_3Player2, 2, 1);
            this.tableLayoutPanel_AnswersPlayer2.Controls.Add(this.button_4Player2, 1, 2);
            this.tableLayoutPanel_AnswersPlayer2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel_AnswersPlayer2.Location = new System.Drawing.Point(0, 92);
            this.tableLayoutPanel_AnswersPlayer2.Name = "tableLayoutPanel_AnswersPlayer2";
            this.tableLayoutPanel_AnswersPlayer2.Padding = new System.Windows.Forms.Padding(50, 0, 50, 0);
            this.tableLayoutPanel_AnswersPlayer2.RowCount = 3;
            this.tableLayoutPanel_AnswersPlayer2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_AnswersPlayer2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_AnswersPlayer2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_AnswersPlayer2.Size = new System.Drawing.Size(226, 87);
            this.tableLayoutPanel_AnswersPlayer2.TabIndex = 2;
            // 
            // button_1Player2
            // 
            this.tableLayoutPanel_AnswersPlayer2.SetColumnSpan(this.button_1Player2, 2);
            this.button_1Player2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_1Player2.Enabled = false;
            this.button_1Player2.Location = new System.Drawing.Point(84, 3);
            this.button_1Player2.Name = "button_1Player2";
            this.button_1Player2.Size = new System.Drawing.Size(56, 23);
            this.button_1Player2.TabIndex = 1;
            this.button_1Player2.TabStop = false;
            this.button_1Player2.Text = "↑";
            this.button_1Player2.UseVisualStyleBackColor = true;
            // 
            // button_2Player2
            // 
            this.tableLayoutPanel_AnswersPlayer2.SetColumnSpan(this.button_2Player2, 2);
            this.button_2Player2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_2Player2.Enabled = false;
            this.button_2Player2.Location = new System.Drawing.Point(53, 32);
            this.button_2Player2.Name = "button_2Player2";
            this.button_2Player2.Size = new System.Drawing.Size(56, 23);
            this.button_2Player2.TabIndex = 1;
            this.button_2Player2.TabStop = false;
            this.button_2Player2.Text = "←";
            this.button_2Player2.UseVisualStyleBackColor = true;
            // 
            // button_3Player2
            // 
            this.tableLayoutPanel_AnswersPlayer2.SetColumnSpan(this.button_3Player2, 2);
            this.button_3Player2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_3Player2.Enabled = false;
            this.button_3Player2.Location = new System.Drawing.Point(115, 32);
            this.button_3Player2.Name = "button_3Player2";
            this.button_3Player2.Size = new System.Drawing.Size(58, 23);
            this.button_3Player2.TabIndex = 1;
            this.button_3Player2.TabStop = false;
            this.button_3Player2.Text = "→";
            this.button_3Player2.UseVisualStyleBackColor = true;
            // 
            // button_4Player2
            // 
            this.tableLayoutPanel_AnswersPlayer2.SetColumnSpan(this.button_4Player2, 2);
            this.button_4Player2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_4Player2.Enabled = false;
            this.button_4Player2.Location = new System.Drawing.Point(84, 61);
            this.button_4Player2.Name = "button_4Player2";
            this.button_4Player2.Size = new System.Drawing.Size(56, 23);
            this.button_4Player2.TabIndex = 1;
            this.button_4Player2.TabStop = false;
            this.button_4Player2.Text = "↓";
            this.button_4Player2.UseVisualStyleBackColor = true;
            // 
            // label_LivesPlayer2
            // 
            this.label_LivesPlayer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_LivesPlayer2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_LivesPlayer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_LivesPlayer2.Location = new System.Drawing.Point(0, 46);
            this.label_LivesPlayer2.Name = "label_LivesPlayer2";
            this.label_LivesPlayer2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.label_LivesPlayer2.Size = new System.Drawing.Size(226, 46);
            this.label_LivesPlayer2.TabIndex = 4;
            this.label_LivesPlayer2.Text = "Vidas: 0";
            this.label_LivesPlayer2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_NamePlayer2
            // 
            this.label_NamePlayer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_NamePlayer2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_NamePlayer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_NamePlayer2.Location = new System.Drawing.Point(0, 0);
            this.label_NamePlayer2.Name = "label_NamePlayer2";
            this.label_NamePlayer2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.label_NamePlayer2.Size = new System.Drawing.Size(226, 46);
            this.label_NamePlayer2.TabIndex = 0;
            this.label_NamePlayer2.Text = "Jugador 2";
            this.label_NamePlayer2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel_Player1
            // 
            this.panel_Player1.AutoSize = true;
            this.panel_Player1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Player1.Controls.Add(this.label_ScorePlayer1);
            this.panel_Player1.Controls.Add(this.tableLayoutPanel_AnswersPlayer1);
            this.panel_Player1.Controls.Add(this.label_LivesPlayer1);
            this.panel_Player1.Controls.Add(this.label_NamePlayer1);
            this.panel_Player1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Player1.Location = new System.Drawing.Point(0, 0);
            this.panel_Player1.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Player1.Name = "panel_Player1";
            this.panel_Player1.Size = new System.Drawing.Size(230, 229);
            this.panel_Player1.TabIndex = 0;
            // 
            // label_ScorePlayer1
            // 
            this.label_ScorePlayer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_ScorePlayer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_ScorePlayer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ScorePlayer1.Location = new System.Drawing.Point(0, 179);
            this.label_ScorePlayer1.Name = "label_ScorePlayer1";
            this.label_ScorePlayer1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.label_ScorePlayer1.Size = new System.Drawing.Size(226, 46);
            this.label_ScorePlayer1.TabIndex = 3;
            this.label_ScorePlayer1.Text = "Puntaje: 0";
            this.label_ScorePlayer1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel_AnswersPlayer1
            // 
            this.tableLayoutPanel_AnswersPlayer1.AutoSize = true;
            this.tableLayoutPanel_AnswersPlayer1.ColumnCount = 4;
            this.tableLayoutPanel_AnswersPlayer1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_AnswersPlayer1.Controls.Add(this.button_1Player1, 1, 0);
            this.tableLayoutPanel_AnswersPlayer1.Controls.Add(this.button_2Player1, 0, 1);
            this.tableLayoutPanel_AnswersPlayer1.Controls.Add(this.button_3Player1, 2, 1);
            this.tableLayoutPanel_AnswersPlayer1.Controls.Add(this.button_4Player1, 1, 2);
            this.tableLayoutPanel_AnswersPlayer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel_AnswersPlayer1.Location = new System.Drawing.Point(0, 92);
            this.tableLayoutPanel_AnswersPlayer1.Name = "tableLayoutPanel_AnswersPlayer1";
            this.tableLayoutPanel_AnswersPlayer1.Padding = new System.Windows.Forms.Padding(50, 0, 50, 0);
            this.tableLayoutPanel_AnswersPlayer1.RowCount = 3;
            this.tableLayoutPanel_AnswersPlayer1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_AnswersPlayer1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_AnswersPlayer1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_AnswersPlayer1.Size = new System.Drawing.Size(226, 87);
            this.tableLayoutPanel_AnswersPlayer1.TabIndex = 2;
            // 
            // button_1Player1
            // 
            this.tableLayoutPanel_AnswersPlayer1.SetColumnSpan(this.button_1Player1, 2);
            this.button_1Player1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_1Player1.Enabled = false;
            this.button_1Player1.Location = new System.Drawing.Point(84, 3);
            this.button_1Player1.Name = "button_1Player1";
            this.button_1Player1.Size = new System.Drawing.Size(56, 23);
            this.button_1Player1.TabIndex = 1;
            this.button_1Player1.TabStop = false;
            this.button_1Player1.Text = "W";
            this.button_1Player1.UseVisualStyleBackColor = true;
            // 
            // button_2Player1
            // 
            this.tableLayoutPanel_AnswersPlayer1.SetColumnSpan(this.button_2Player1, 2);
            this.button_2Player1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_2Player1.Enabled = false;
            this.button_2Player1.Location = new System.Drawing.Point(53, 32);
            this.button_2Player1.Name = "button_2Player1";
            this.button_2Player1.Size = new System.Drawing.Size(56, 23);
            this.button_2Player1.TabIndex = 1;
            this.button_2Player1.TabStop = false;
            this.button_2Player1.Text = "A";
            this.button_2Player1.UseVisualStyleBackColor = true;
            // 
            // button_3Player1
            // 
            this.tableLayoutPanel_AnswersPlayer1.SetColumnSpan(this.button_3Player1, 2);
            this.button_3Player1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_3Player1.Enabled = false;
            this.button_3Player1.Location = new System.Drawing.Point(115, 32);
            this.button_3Player1.Name = "button_3Player1";
            this.button_3Player1.Size = new System.Drawing.Size(58, 23);
            this.button_3Player1.TabIndex = 1;
            this.button_3Player1.TabStop = false;
            this.button_3Player1.Text = "D";
            this.button_3Player1.UseVisualStyleBackColor = true;
            // 
            // button_4Player1
            // 
            this.tableLayoutPanel_AnswersPlayer1.SetColumnSpan(this.button_4Player1, 2);
            this.button_4Player1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_4Player1.Enabled = false;
            this.button_4Player1.Location = new System.Drawing.Point(84, 61);
            this.button_4Player1.Name = "button_4Player1";
            this.button_4Player1.Size = new System.Drawing.Size(56, 23);
            this.button_4Player1.TabIndex = 1;
            this.button_4Player1.TabStop = false;
            this.button_4Player1.Text = "S";
            this.button_4Player1.UseVisualStyleBackColor = true;
            // 
            // label_LivesPlayer1
            // 
            this.label_LivesPlayer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_LivesPlayer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_LivesPlayer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_LivesPlayer1.Location = new System.Drawing.Point(0, 46);
            this.label_LivesPlayer1.Name = "label_LivesPlayer1";
            this.label_LivesPlayer1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.label_LivesPlayer1.Size = new System.Drawing.Size(226, 46);
            this.label_LivesPlayer1.TabIndex = 4;
            this.label_LivesPlayer1.Text = "Vidas: 0";
            this.label_LivesPlayer1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_NamePlayer1
            // 
            this.label_NamePlayer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_NamePlayer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_NamePlayer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_NamePlayer1.Location = new System.Drawing.Point(0, 0);
            this.label_NamePlayer1.Name = "label_NamePlayer1";
            this.label_NamePlayer1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.label_NamePlayer1.Size = new System.Drawing.Size(226, 46);
            this.label_NamePlayer1.TabIndex = 0;
            this.label_NamePlayer1.Text = "Jugador 1";
            this.label_NamePlayer1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel_Questions
            // 
            this.panel_Questions.AutoSize = true;
            this.panel_Questions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panel_Questions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Questions.Controls.Add(this.tableLayoutPanel_Answers);
            this.panel_Questions.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Questions.Location = new System.Drawing.Point(25, 124);
            this.panel_Questions.Name = "panel_Questions";
            this.panel_Questions.Padding = new System.Windows.Forms.Padding(50, 25, 50, 25);
            this.panel_Questions.Size = new System.Drawing.Size(460, 171);
            this.panel_Questions.TabIndex = 0;
            // 
            // tableLayoutPanel_Answers
            // 
            this.tableLayoutPanel_Answers.AutoSize = true;
            this.tableLayoutPanel_Answers.ColumnCount = 4;
            this.tableLayoutPanel_Answers.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_Answers.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_Answers.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_Answers.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_Answers.Controls.Add(this.label_Answer1, 1, 0);
            this.tableLayoutPanel_Answers.Controls.Add(this.label_Answer2, 0, 1);
            this.tableLayoutPanel_Answers.Controls.Add(this.label_Answer4, 1, 2);
            this.tableLayoutPanel_Answers.Controls.Add(this.label_Answer3, 2, 1);
            this.tableLayoutPanel_Answers.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel_Answers.Location = new System.Drawing.Point(50, 25);
            this.tableLayoutPanel_Answers.Name = "tableLayoutPanel_Answers";
            this.tableLayoutPanel_Answers.RowCount = 3;
            this.tableLayoutPanel_Answers.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Answers.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Answers.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Answers.Size = new System.Drawing.Size(356, 117);
            this.tableLayoutPanel_Answers.TabIndex = 1;
            // 
            // label_Answer1
            // 
            this.label_Answer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel_Answers.SetColumnSpan(this.label_Answer1, 2);
            this.label_Answer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label_Answer1.Location = new System.Drawing.Point(92, 5);
            this.label_Answer1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label_Answer1.Name = "label_Answer1";
            this.label_Answer1.Size = new System.Drawing.Size(172, 28);
            this.label_Answer1.TabIndex = 2;
            this.label_Answer1.Text = "Respuesta 1";
            this.label_Answer1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Answer2
            // 
            this.label_Answer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel_Answers.SetColumnSpan(this.label_Answer2, 2);
            this.label_Answer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label_Answer2.Location = new System.Drawing.Point(3, 44);
            this.label_Answer2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label_Answer2.Name = "label_Answer2";
            this.label_Answer2.Size = new System.Drawing.Size(172, 28);
            this.label_Answer2.TabIndex = 2;
            this.label_Answer2.Text = "Respuesta 2";
            this.label_Answer2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Answer4
            // 
            this.label_Answer4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel_Answers.SetColumnSpan(this.label_Answer4, 2);
            this.label_Answer4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label_Answer4.Location = new System.Drawing.Point(92, 83);
            this.label_Answer4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label_Answer4.Name = "label_Answer4";
            this.label_Answer4.Size = new System.Drawing.Size(172, 29);
            this.label_Answer4.TabIndex = 2;
            this.label_Answer4.Text = "Respuesta 4";
            this.label_Answer4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Answer3
            // 
            this.label_Answer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel_Answers.SetColumnSpan(this.label_Answer3, 2);
            this.label_Answer3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label_Answer3.Location = new System.Drawing.Point(181, 44);
            this.label_Answer3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label_Answer3.Name = "label_Answer3";
            this.label_Answer3.Size = new System.Drawing.Size(172, 28);
            this.label_Answer3.TabIndex = 2;
            this.label_Answer3.Text = "Respuesta 3";
            this.label_Answer3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Question
            // 
            this.label_Question.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_Question.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_Question.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.label_Question.Location = new System.Drawing.Point(25, 62);
            this.label_Question.Name = "label_Question";
            this.label_Question.Size = new System.Drawing.Size(460, 62);
            this.label_Question.TabIndex = 1;
            this.label_Question.Text = "¿Cuál será la proxima pregunta?";
            this.label_Question.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel_RoundInfo
            // 
            this.tableLayoutPanel_RoundInfo.AutoSize = true;
            this.tableLayoutPanel_RoundInfo.ColumnCount = 2;
            this.tableLayoutPanel_RoundInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_RoundInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_RoundInfo.Controls.Add(this.label_Category, 0, 0);
            this.tableLayoutPanel_RoundInfo.Controls.Add(this.label_TimeLeft, 1, 0);
            this.tableLayoutPanel_RoundInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel_RoundInfo.Location = new System.Drawing.Point(25, 25);
            this.tableLayoutPanel_RoundInfo.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_RoundInfo.Name = "tableLayoutPanel_RoundInfo";
            this.tableLayoutPanel_RoundInfo.RowCount = 1;
            this.tableLayoutPanel_RoundInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_RoundInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_RoundInfo.Size = new System.Drawing.Size(460, 37);
            this.tableLayoutPanel_RoundInfo.TabIndex = 2;
            // 
            // label_Category
            // 
            this.label_Category.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_Category.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_Category.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label_Category.Location = new System.Drawing.Point(0, 0);
            this.label_Category.Margin = new System.Windows.Forms.Padding(0);
            this.label_Category.Name = "label_Category";
            this.label_Category.Size = new System.Drawing.Size(230, 37);
            this.label_Category.TabIndex = 0;
            this.label_Category.Text = "Categoría";
            this.label_Category.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_TimeLeft
            // 
            this.label_TimeLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_TimeLeft.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_TimeLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label_TimeLeft.Location = new System.Drawing.Point(230, 0);
            this.label_TimeLeft.Margin = new System.Windows.Forms.Padding(0);
            this.label_TimeLeft.Name = "label_TimeLeft";
            this.label_TimeLeft.Size = new System.Drawing.Size(230, 37);
            this.label_TimeLeft.TabIndex = 1;
            this.label_TimeLeft.Text = "Tiempo Restante: 0s";
            this.label_TimeLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Form_Game
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(514, 586);
            this.ControlBox = false;
            this.Controls.Add(this.panel_GameView);
            this.MaximumSize = new System.Drawing.Size(530, 625);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(530, 625);
            this.Name = "Form_Game";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Pregunta3";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel_GameView.ResumeLayout(false);
            this.panel_GameView.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel_PlayersView.ResumeLayout(false);
            this.tableLayoutPanel_PlayersView.PerformLayout();
            this.panel_Player2.ResumeLayout(false);
            this.panel_Player2.PerformLayout();
            this.tableLayoutPanel_AnswersPlayer2.ResumeLayout(false);
            this.panel_Player1.ResumeLayout(false);
            this.panel_Player1.PerformLayout();
            this.tableLayoutPanel_AnswersPlayer1.ResumeLayout(false);
            this.panel_Questions.ResumeLayout(false);
            this.panel_Questions.PerformLayout();
            this.tableLayoutPanel_Answers.ResumeLayout(false);
            this.tableLayoutPanel_RoundInfo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel_GameView;
        private System.Windows.Forms.Panel panel_Questions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_PlayersView;
        private System.Windows.Forms.Panel panel_Player1;
        private System.Windows.Forms.Button button_1Player1;
        private System.Windows.Forms.Label label_NamePlayer1;
        private System.Windows.Forms.Button button_2Player1;
        private System.Windows.Forms.Button button_3Player1;
        private System.Windows.Forms.Button button_4Player1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Answers;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_AnswersPlayer1;
        private System.Windows.Forms.Label label_ScorePlayer1;
        private System.Windows.Forms.Panel panel_Player2;
        private System.Windows.Forms.Label label_ScorePlayer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_AnswersPlayer2;
        private System.Windows.Forms.Button button_1Player2;
        private System.Windows.Forms.Button button_2Player2;
        private System.Windows.Forms.Button button_3Player2;
        private System.Windows.Forms.Button button_4Player2;
        private System.Windows.Forms.Label label_NamePlayer2;
        private System.Windows.Forms.Label label_Question;
        private System.Windows.Forms.Label label_Answer1;
        private System.Windows.Forms.Label label_Answer2;
        private System.Windows.Forms.Label label_Answer4;
        private System.Windows.Forms.Label label_Answer3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_RoundInfo;
        private System.Windows.Forms.Label label_Category;
        private System.Windows.Forms.Label label_TimeLeft;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button_Play;
        private System.Windows.Forms.Label label_LivesPlayer2;
        private System.Windows.Forms.Label label_LivesPlayer1;
        private System.Windows.Forms.Timer timer;
    }
}

