﻿namespace P3.UI
{
    partial class Form_MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_MainScreen));
            this.toolStrip_MainMenu = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_Settings = new System.Windows.Forms.ToolStripButton();
            this.toolStrip_MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip_MainMenu
            // 
            this.toolStrip_MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Settings});
            this.toolStrip_MainMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_MainMenu.Name = "toolStrip_MainMenu";
            this.toolStrip_MainMenu.Size = new System.Drawing.Size(514, 25);
            this.toolStrip_MainMenu.TabIndex = 1;
            this.toolStrip_MainMenu.Text = "toolStrip_MainMenu";
            // 
            // toolStripButton_Settings
            // 
            this.toolStripButton_Settings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Settings.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Settings.Image")));
            this.toolStripButton_Settings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Settings.Name = "toolStripButton_Settings";
            this.toolStripButton_Settings.Size = new System.Drawing.Size(117, 22);
            this.toolStripButton_Settings.Text = "Opciones de Partida";
            this.toolStripButton_Settings.Click += new System.EventHandler(this.toolStripButton_Settings_Click);
            // 
            // Form_MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 636);
            this.Controls.Add(this.toolStrip_MainMenu);
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(530, 675);
            this.Name = "Form_MainScreen";
            this.Text = "Pregunta3";
            this.Load += new System.EventHandler(this.Form_MainScreen_Load);
            this.toolStrip_MainMenu.ResumeLayout(false);
            this.toolStrip_MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_MainMenu;
        private System.Windows.Forms.ToolStripButton toolStripButton_Settings;
    }
}