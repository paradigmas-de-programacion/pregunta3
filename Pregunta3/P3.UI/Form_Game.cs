﻿using Microsoft.VisualBasic;
using P3.APP;
using P3.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace P3.UI
{
    public partial class Form_Game : Form
    {
        public GameManager GM;
        DefaultFields DF = new DefaultFields();

        public Form_Game()
        {
            InitializeComponent();
            GM = new GameManager();
            GM.Tick += (sender, args) => { UpdateTimer(); };
            GM.GameStarted += (sender, args) =>
            {
                timer.Enabled = true;
                Form_MainScreen temp = (Form_MainScreen)ParentForm;
                temp.DisableSettings();
                UpdateUI();
            };
            GM.RoundStart += (sender, args) =>
            {
                EnablePlayer(tableLayoutPanel_AnswersPlayer1);
                EnablePlayer(tableLayoutPanel_AnswersPlayer2);
                UpdateUI();
                button_Play.Text = "Partida en curso";
            };
            GM.RoundEnded += (sender, args) =>
            {
                UpdateUI();
                EndRound();
            };
            GM.GameFinished += (sender, args) =>
            {
                EndGame();
                Form_MainScreen temp = (Form_MainScreen)ParentForm;
                temp.EnableSettings();
            };
        }

        private void button_PlayPause_Click(object sender, EventArgs e)
        {
            if (GM.Players.Count != 2)
            {
                CreatePlayers();
                UpdatePlayers();
                button_Play.Text = "Comenzar Partida";
                SaveDefaultFields();
            }
            else
            {
                button_Play.Click -= new EventHandler(button_PlayPause_Click);
                GM.TimerTick();
            }
        }
        private void button_Play_KeyDown(object sender, KeyEventArgs e)
        {
            if (GM.InGame)
            {
                switch (e.KeyCode)
                {
                    case Keys.W:
                        if (GM.Players[0].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer1);
                        }
                        GM.AnsweredQuestion(GM.Players[0], GM.CurrentQuestion.Answers[0]);
                        break;
                    case Keys.A:
                        if (GM.Players[0].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer1);
                        }
                        GM.AnsweredQuestion(GM.Players[0], GM.CurrentQuestion.Answers[1]);
                        break;
                    case Keys.S:
                        if (GM.Players[0].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer1);
                        }
                        GM.AnsweredQuestion(GM.Players[0], GM.CurrentQuestion.Answers[2]);
                        break;
                    case Keys.D:
                        if (GM.Players[0].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer1);
                        }
                        GM.AnsweredQuestion(GM.Players[0], GM.CurrentQuestion.Answers[3]);
                        break;
                    case Keys.I:
                        if (GM.Players[1].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer2);
                        }
                        GM.AnsweredQuestion(GM.Players[1], GM.CurrentQuestion.Answers[0]);
                        break;
                    case Keys.J:
                        if (GM.Players[1].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer2);
                        }
                        GM.AnsweredQuestion(GM.Players[1], GM.CurrentQuestion.Answers[1]);
                        break;
                    case Keys.L:
                        if (GM.Players[1].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer2);
                        }
                        GM.AnsweredQuestion(GM.Players[1], GM.CurrentQuestion.Answers[2]);
                        break;
                    case Keys.K:
                        if (GM.Players[1].Enabled)
                        {
                            DisablePlayer(tableLayoutPanel_AnswersPlayer2);
                        }
                        GM.AnsweredQuestion(GM.Players[1], GM.CurrentQuestion.Answers[3]);
                        break;
                    default:
                        break;
                }
            }
        }

        private void EnablePlayer(TableLayoutPanel panel)
        {
            panel.BackColor = GM.Settings.DefaultBackColor;
        }
        private void DisablePlayer(TableLayoutPanel panel)
        {
            panel.BackColor = Color.LightGray;
        }
        private void ShowCorrectAnswers(int i, TableLayoutPanel panel)
        {
            if (GM.Players[i].AnsweredCorrectly)
            {
                panel.BackColor = Color.LightGreen;
            }
            else
            {
                panel.BackColor = Color.Firebrick;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            GM.TimerTick();
        }

        private void EndRound()
        {
            ShowCorrectAnswers(0, tableLayoutPanel_AnswersPlayer1);
            ShowCorrectAnswers(1, tableLayoutPanel_AnswersPlayer2);
        }
        private void EndGame()
        {
            timer.Stop(); timer.Enabled = false;
            switch (GM.Winner)
            {
                case null:
                    MessageBox.Show($"El resultado ha sido Empate!\nPulsa ok para continuar!", "Partida Terminada", MessageBoxButtons.OK);
                    break;
                default:
                    MessageBox.Show($"El ganador de la partida es {GM.Winner.Name}!\nPulsa ok para continuar!", "Partida Terminada", MessageBoxButtons.OK);
                    break;
            }
            UpdateUI();
            button_Play.Click += new EventHandler(button_PlayPause_Click);
            button_Play.Text = "Comenzar Partida";
        }

        private void CreatePlayers()
        {
            Player[] tempPlayers = new Player[2];
            tempPlayers[0] = new Player(Interaction.InputBox("Ingrese el nombre del Jugador 1", "Nuevo Juego", "Jugador 1"));
            tempPlayers[1] = new Player(Interaction.InputBox("Ingrese el nombre del Jugador 2", "Nuevo Juego", "Jugador 2"));
            if (tempPlayers[0].Name == "") { tempPlayers[0].Name = "Jugador 1"; }
            if (tempPlayers[1].Name == "") { tempPlayers[1].Name = "Jugador 2"; }
            GM.CreatePlayers(tempPlayers[0], tempPlayers[1]);
        }
        private void SaveDefaultFields()
        {
            DF.label_Default_Category = label_Category.Text;
            DF.label_Default_Question = label_Question.Text;
            DF.label_Default_Answer1 = label_Answer1.Text;
            DF.label_Default_Answer2 = label_Answer2.Text;
            DF.label_Default_Answer3 = label_Answer3.Text;
            DF.label_Default_Answer4 = label_Answer4.Text;
            DF.button_Default_Play = button_Play.Text;
        }
        private void UpdateUI()
        {
            UpdateQuestions();
            UpdatePlayers();
        }
        private void UpdateQuestions()
        {
            if (!GM.InGame)
            {
                label_Category.Text = DF.label_Default_Category;
                label_Question.Text = DF.label_Default_Question;
                label_Answer1.Text = DF.label_Default_Answer1;
                label_Answer2.Text = DF.label_Default_Answer2;
                label_Answer3.Text = DF.label_Default_Answer3;
                label_Answer4.Text = DF.label_Default_Answer4;
                button_Play.Text = DF.button_Default_Play;
            }
            else
            {
                label_Category.Text = GM.CurrentQuestion.CategoryName;
                label_Question.Text = GM.CurrentQuestion.Text;
                label_Answer1.Text = GM.CurrentQuestion.Answers[0].Text;
                label_Answer2.Text = GM.CurrentQuestion.Answers[1].Text;
                label_Answer3.Text = GM.CurrentQuestion.Answers[2].Text;
                label_Answer4.Text = GM.CurrentQuestion.Answers[3].Text;
            }
        }
        private void UpdatePlayers()
        {
            label_NamePlayer1.Text = GM.Players[0].Name;
            label_ScorePlayer1.Text = "Puntaje: "+GM.Players[0].Score.ToString();
            label_LivesPlayer1.Text = "Vidas: "+GM.Players[0].Lives.ToString();
            tableLayoutPanel_AnswersPlayer1.BackColor = GM.Settings.DefaultBackColor;
            label_NamePlayer2.Text = GM.Players[1].Name;
            label_ScorePlayer2.Text = "Puntaje: " + GM.Players[1].Score.ToString();
            label_LivesPlayer2.Text = "Vidas: " + GM.Players[1].Lives.ToString();
            tableLayoutPanel_AnswersPlayer2.BackColor = GM.Settings.DefaultBackColor;
        }
        private void UpdateTimer()
        {
            label_TimeLeft.Text = "Tiempo Restante: "+GM.SecondsLeft.ToString()+"s";
        }
    }

    public class DefaultFields
    {
        public string label_Default_Category;
        public string label_Default_Question;
        public string label_Default_Answer1;
        public string label_Default_Answer2;
        public string label_Default_Answer3;
        public string label_Default_Answer4;
        public string button_Default_Play;
    }
}
