﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P3.BLL
{
    public class Question
    {
        public string CategoryName { get; private set; }
        public string Text { get; private set; }
        public List<Answer> Answers {  get; private set; }
        public int Id { get; private set; }

        public Question(int id, string category, string text)
        {
            Id = id;
            CategoryName = category;
            Text = text;
        }
        public Question(int id, string category, string text, List<Answer> answers)
        {
            Id = id;
            CategoryName = category;
            Text = text;
            Answers = answers;
        }
        public void FillAnswers(List<Answer> answers)
        {
            Answers = answers;
        }
    }

    public class Answer
    {
        public string Text { get; private set; }
        public bool IsCorrect { get; private set; }
        
        public Answer(string text, bool isCorrect)
        {
            Text = text;
            IsCorrect = isCorrect;
        }
    }
}
