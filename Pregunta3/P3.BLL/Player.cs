﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P3.BLL
{
    public class Player
    {
        public string Name { get; set; }
        public int Score { get; set; }
        public int Lives {  get; set; }
        public bool Enabled { get; set; }
        public bool AnsweredCorrectly { get; set; } = false;
        
        public Player(string name)
        {
            Name=name;
            Score = 0;
            Lives = 3;
            Enabled = false;
            AnsweredCorrectly = false;
        }
        public Player(string name, int lives)
        {
            Name = name;
            Score = 0;
            Lives = lives;
            Enabled = false;
            AnsweredCorrectly = false;
        }
    }
}
