﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using P3.BLL;
using System.Runtime.Remoting.Messaging;


namespace P3.DB
{
    public class DBAccess
    {
        private  SqlConnection sqlConn { get; set; }

        public DBAccess()
        {
            sqlConn = new SqlConnection(Properties.Settings.Default.connString);
        }

        public List<Question> GetQuestionsBatch(int amount)
        {
            List<Question> questions = new List<Question>();
            using (SqlConnection sqlConn = new SqlConnection(Properties.Settings.Default.connString))
            {
                //Search Questions
                sqlConn.Open();
                using (SqlCommand cmd = new SqlCommand($"select top {amount} Q.ID, C.CategoryName, Q.QuestionText from Questions Q inner join Categories C on Q.CategoryId = C.Id order by NEWID()",sqlConn))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            questions.Add(new Question(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
                        }
                    }
                }
                //Search Answers
                foreach (Question q in questions)
                {
                    using (SqlCommand cmd = new SqlCommand($"select AnswerText, IsCorrect from Answers where QuestionId = {q.Id} order by NEWID()", sqlConn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            List<Answer> answers = new List<Answer>();
                            while (reader.Read())
                            {
                                answers.Add(new Answer(reader.GetString(0), reader.GetBoolean(1)));
                            }
                            q.FillAnswers(answers);
                        }
                    }
                }
                sqlConn.Close();
            }
            return questions;
        }

        public int GetQuestionsQuantity()
        {
            int amount = 0;
            using (SqlConnection sqlConn = new SqlConnection(Properties.Settings.Default.connString))
            {
                sqlConn.Open();
                using (SqlCommand cmd = new SqlCommand($"select count(Id) as amount from Questions", sqlConn))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        amount = reader.GetInt32(0);
                    }
                }
                sqlConn.Close();
            }
            return amount;
        }

    }
}
