﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Timers;
using Microsoft.VisualBasic;
using P3.BLL;
using P3.DB;

namespace P3.APP
{
    public class GameManager
    {
        //Game
        private DBAccess DBManager { get; set; }
        public GameSettings Settings { get; set; }
        public List<Player> Players { get; set; }
        public Player Winner { get; set; }
        public bool InGame { get; private set; }
        public bool InPause {  get; private set; }

        //Round
        public Question CurrentQuestion { get; private set; }
        private int CurrentQuestionNumber { get; set; }
        private List<Question> QuestionsBatch { get; set; }
        public int SecondsLeft {  get; set; }

        //Tick Event
        public delegate void TickDelegate(object sender, EventArgs args);
        public event EventHandler Tick = delegate { };
        //Start Game Event
        public delegate void GameStartedDelegate(object sender, EventArgs args);
        public event EventHandler GameStarted = delegate { };
        //New Round Event
        public delegate void RoundStartedRoundDelegate(object sender, EventArgs args);
        public event EventHandler RoundStart = delegate { };
        //End Round Event
        public delegate void RoundEndedDelegate(object sender, EventArgs args);
        public event EventHandler RoundEnded = delegate { };
        //End Game Event
        public delegate void GameFinishedDelegate(object sender, EventArgs args);
        public event EventHandler GameFinished = delegate { };

        //Constructor
        public GameManager()
        {
            DBManager = new DBAccess();
            Settings = new GameSettings();
            InGame = false;
            Players = new List<Player>();
            Settings.UpdateQuestionsAmount(DBManager.GetQuestionsQuantity());
        }

        //Game Variables
        public void CreatePlayers(Player p1, Player p2)
        {
            if (Players.Count != 2)
            {
                Players.Add(new Player(p1.Name,Settings.LivesPerGame));
                Players.Add(new Player(p2.Name,Settings.LivesPerGame));
            } else
            {
                foreach(Player p in Players)
                {
                    p.Lives = Settings.LivesPerGame;
                }
            }
        }

        //Game Loop
        public void TimerTick()
        {
            //If the game has not started
            if (!InGame)
            {
                //If the game (not started) is not paused, start the game
                if (!InPause)
                {
                    NewGame();
                }
                //If the game (not started) is paused, countdown until first round
                else
                {
                    //If the countdown is done, start the next round
                    if (SecondsLeft <= 0)
                    {
                        NewRound();
                    }
                    //If not, reduce timer
                    else
                    {
                        SecondsLeft--;
                    }
                }
            }
            //If the game has started
            else
            {
                //If a round has ended, 
                if (InPause)
                {
                    //If there are questions and lives left for all players, go to the next round
                    if ((CurrentQuestionNumber < QuestionsBatch.Count) && (Players[0].Lives > 0) && (Players[1].Lives > 0))
                    {
                        NewRound();
                    }
                    //If not, End the game
                    else
                    {
                        EndGame();
                    }
                }
                //If the round is still going, if the time has ran out, end the round
                else if (SecondsLeft <= 0)
                {
                    EndRound();
                }
                //If the round is still going, reduce timer
                else
                {
                    SecondsLeft--;
                }
            }
            Tick(this, new EventArgs());
        }

        public void NewGame()
        {
            //Set Lives to Max amount of lives
            CreatePlayers(Players[0], Players[1]);
            //Get Questions
            QuestionsBatch = new List<Question>(DBManager.GetQuestionsBatch(Settings.QuestionsPerGame));
            CurrentQuestionNumber = 0;
            //Set GameState
            InGame = false; InPause = true;
            //Set Time Left
            SecondsLeft = 3;
            //Game Start Event
            GameStarted(this, new EventArgs());
        }
        public void NewRound()
        {
            //Unpause and Start Game
            InPause = false; InGame = true;
            //Enable Players
            foreach (Player p in Players)
            {
                p.Enabled = true;
                p.AnsweredCorrectly = false;
            }
            //Set Current Question
            CurrentQuestion = QuestionsBatch[CurrentQuestionNumber];
            //Prepare Next Question
            CurrentQuestionNumber++;
            //Set Time Left
            SecondsLeft = (int)Settings.TimePerRound;
            //Round Start Event
            RoundStart(this, new EventArgs());
        }
        public void AnsweredQuestion(Player player, Answer answer)
        {
            //If the player is enabled, disable it and mark it as correct/incorrect
            if (player.Enabled)
            {
                player.Enabled = false;
                if (answer.IsCorrect)
                {
                    player.AnsweredCorrectly = true;
                }
                else
                {
                    player.AnsweredCorrectly = false;
                }
            }
        }
        public void EndRound()
        {
            //Pause Game
            InPause = true;
            //Check Players Answers
            foreach (Player p in Players)
            {
                if (p.AnsweredCorrectly)
                {
                    p.Score++;
                }
                else
                {
                    p.Lives--;
                }
            }
            //Round End Event
            RoundEnded(this, new EventArgs());
        }
        public void EndGame()
        {
            //End Game
            InGame = false; InPause = false;
            //Disable Players
            foreach (Player p in Players)
            {
                p.Enabled = false;
                p.AnsweredCorrectly = false;
            }
            //Check the Results
            CheckResults();
            //Game End Event
            GameFinished(this, new EventArgs());
        }

        private void CheckResults()
        {
            if (Players[0].Lives == 0 && Players[1].Lives == 0)
            {
                Winner = null;
            }
            else if (Players[0].Lives <= 0)
            {
                Winner = Players[1];
            }
            else if (Players[1].Lives <= 0)
            {
                Winner = Players[0];
            }
            else if (Players[0].Score == Players[1].Score)
            {
                Winner = null;
            }
            else if (Players[0].Score > Players[1].Score)
            {
                Winner = Players[0];
            }
            else if (Players[0].Score < Players[1].Score)
            {
                Winner = Players[1];
            }
        }
    }
}