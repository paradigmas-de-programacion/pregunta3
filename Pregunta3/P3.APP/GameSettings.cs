﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P3.APP
{
    public class GameSettings
    {
        public int QuestionsPerGame { get; private set; }
        public double TimePerRound { get; private set; }
        public int LivesPerGame {  get; private set; }
        public Color DefaultBackColor { get; private set; }
        public int QuestionsAmount { get; private set; }

        public GameSettings()
        {
            QuestionsPerGame = 10;
            TimePerRound = 10;
            LivesPerGame = 3;
            DefaultBackColor = Color.FromArgb(255, 192, 128);
        }

        public void UpdateQuestionsPerGame(int amount)
        {
            QuestionsPerGame = amount;
        }
        public void UpdateTimePerRound(int amount)
        {
            TimePerRound = (double)amount;
        }
        public void UpdateLivesPerGame(int amount)
        {
            LivesPerGame = amount;
        }
        public void UpdateQuestionsAmount(int amount)
        {
            QuestionsAmount = amount;
        }
    }
}
